
#Implements the main DTL algorithm.
# The class should somehow be encoded in the datapoints
#  structure. A reasonable approach would be to use
#  the feature name "class" to refer to it consistently.
def DTL(datapoints, features, default_value):
    pass

#Returns the feature with the greatest information
# gain for the provided datapoints.
def Choose_Attribute(features, datapoints):
    pass

#Returns a set of subsets of datapoints
# such that all points in a subset have
# the same value of the indicated feature.
def Make_Subsets(datapoints, feature):
    pass

#Computes the mode of the class of datapoints.
def Compute_Mode(datapoints):
    pass

#Computes the information entropy of datapoints w.r.t
# the class.
def Compute_Information(datapoints):
    pass
